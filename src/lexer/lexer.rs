use std::str::Chars;

use crate::location::Location;
use super::token::{Token, TokenType, is_sign};
use std::vec::Vec;
use std::fmt;

#[derive(Debug)]
pub enum LexerError
{
	NotANumber(String, Location),
}

impl fmt::Display for LexerError
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		match self
		{
			LexerError::NotANumber(word, location) =>
				write!(f, "{} isn't a number {}", word, location)
		}
	}

}

pub fn lexe<'a>(
	source: &'a str)
	-> Result<Vec<Token>, LexerError>
{

	let location = Location {
		position: 0,
		line: 1
	};

	return loop_charcters(
		source.chars(),
		vec![],
		vec![],
		None,
		location
	);
}

fn loop_charcters(
	mut source_code: Chars,
	mut token_stream: Vec<Token>,
	mut word: Vec<char>,
	mut start_word_location: Option<Location>,
	mut location: Location
) -> Result<Vec<Token>, LexerError>
{
	let next = source_code.next();
	if next.is_none()
	{
		update(
			&mut word,
			&mut start_word_location,
			&mut token_stream
		)?;
		return Ok(token_stream);
	}
	location.position += 1;
	let character = next.unwrap();
	if character == '\n'
	{
		update(
			&mut word,
			&mut start_word_location,
			&mut token_stream
		)?;
		token_stream.push((
			TokenType::NewLine,
			location.clone()
		));
		location.line += 1;
		location.position = 0;
		return loop_charcters(
			source_code,
			token_stream,
			word,
			start_word_location,
			location
		);
	} else if character.is_whitespace()
	{
		update(
			&mut word,
			&mut start_word_location,
			&mut token_stream
		)?;
		return loop_charcters(
			source_code,
			token_stream,
			word,
			start_word_location,
			location
		);
	}else if !reserved(&character)
	{
		word.push(character);
		if start_word_location.is_none() {
			start_word_location = Some(location.clone())
		}
		return loop_charcters(
			source_code,
			token_stream,
			word,
			start_word_location,
			location
		);
	}
	update(
		&mut word,
		&mut start_word_location,
		&mut token_stream
	)?;
	match character
	{
		':' => token_stream.push((
			TokenType::SignatureSeparator,
			location.clone()
		)),
		',' => token_stream.push((
			TokenType::ListSeparator,
			location.clone()
		)),
		'=' => token_stream.push((
			TokenType::Assignment,
			location.clone()
		)),
		'.' => token_stream.push((
			TokenType::InfixModifier,
			location.clone()
		)),
		_ => ()
	}
	return loop_charcters(
		source_code,
		token_stream,
		word,
		start_word_location,
		location
	)
}

fn update(
	word: &mut Vec<char>,
	word_location: &mut Option<Location>,
	token_stream: &mut Vec<Token>
) -> Result<(), LexerError>
{
	if word.is_empty()
	{
		return Ok(());
	}

	let location: Location = word_location
		.as_mut()
		.unwrap()
		.clone();
	let token = lexe_word(
		word.clone(),
		location
	)?;

	token_stream.push(token);
	word.clear();
	*word_location = None;
	Ok(())
}

fn lexe_word(
	word: Vec<char>,
	location: Location
) -> Result<Token, LexerError>
{
	if (word.len() > 1 && is_sign(word[0])) ||
		word[0].is_numeric()
	{
		return lexe_int(
			word,
			location
		)
	}
	return Ok((
		TokenType::Word(
			word
				.iter()
				.collect()),
		location
	))
}

fn reserved(
	c: &char
) -> bool
{
	match c
	{
		':' => true,
		',' => true,
		'=' => true,
		'.' => true,
		_ => false
	}
}

fn lexe_int(
	word: Vec<char>,
	location: Location,
) -> Result<Token, LexerError>
{
	word
		.iter()
		.filter(|c| **c != '_')
		.collect::<String>()
		.parse::<i32>()
		.map(|number| (
			TokenType::Number(number),
			location.clone())
		).map_err(|_| LexerError::NotANumber(
			word
				.iter()
				.collect(),
			location
		))
}

#[cfg(test)]
mod test
{
	use crate::{location::Location, lexer::token::TokenType};

	use super::*;


	#[test]
	fn new_line_multyline()
	{
		let result = lexe("main = 1


two = 2");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("main".to_string()),
				Location {
					position:1,
					line: 1
				}),
				(
					TokenType::Assignment,
					Location {
						position: 6,
						line: 1
					}),
				(
					TokenType::Number(1),
					Location {
						position:8,
						line: 1
					}),
				(
					TokenType::NewLine,
					Location {
						position: 9,
						line: 1
					}),
				(
					TokenType::NewLine,
					Location {
						position: 1,
						line: 2
					}),
				(
					TokenType::NewLine,
					Location {
						position: 1,
						line: 3
					}),
				(
					TokenType::Word("two".to_string()),
					Location {
						position:1,
						line: 4
					}),
				(
					TokenType::Assignment,
					Location {
						position: 5,
						line: 4
					}),
				(
					TokenType::Number(2),
					Location {
						position:7,
						line: 4
					})]
		);
	}

	#[test]
	fn new_line_manuel()
	{

		let result = lexe("main = 1\ntwo = 2");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("main".to_string()),
				Location {
					position:1,
					line: 1
				}),
				(
					TokenType::Assignment,
					Location {
						position: 6,
						line: 1
					}),
				(
					TokenType::Number(1),
					Location {
						position:8,
						line: 1
					}),
				(
					TokenType::NewLine,
					Location {
						position: 9,
						line: 1
					}),
				(
					TokenType::Word("two".to_string()),
					Location {
						position:1,
						line: 2
					}),
				(
					TokenType::Assignment,
					Location {
						position: 5,
						line: 2
					}),
				(
					TokenType::Number(2),
					Location {
						position:7,
						line: 2
					})]
		);
	}

	#[test]
	fn words()
	{
		let mut result = lexe("one");
		assert!(result.is_ok());
		assert_eq!(
			result
				.unwrap(),
			vec![(
				TokenType::Word("one".to_string()),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("two");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("two".to_string()),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("one1 two2");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("one1".to_string()),
				Location {
					position:1,
					line: 1
				}),
				(
					TokenType::Word(
						"two2".to_string()),
					Location {
						position:6,
						line: 1
					})]
		);

		result = lexe("one1 \n two2");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("one1".to_string()),
				Location {
					position:1,
					line: 1
				}),
				(
					TokenType::NewLine,
					Location {
						position: 6,
						line: 1
					}),
				(
					TokenType::Word("two2".to_string()),
					Location {
						position:2,
						line: 2
					})]
		);
	}

	#[test]
	fn reserved()
	{
		let result = lexe(":,=.");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::SignatureSeparator,
				Location {
					position:1,
					line: 1
				}),
				(
					TokenType::ListSeparator,
					Location {
						position:2,
						line: 1
					}),
				(
					TokenType::Assignment,
					Location {
						position:3,
						line: 1
					}),
				(
					TokenType::InfixModifier,
					Location {
						position:4,
						line: 1
					})
			]
		);
	}

	#[test]
	fn signs()
	{
		let mut result = lexe("+");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("+".to_string()),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("-");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("-".to_string()),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("- +");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Word("-".to_string()),
				Location {
					position:1,
					line: 1
				}),
				(
					TokenType::Word("+".to_string()),
					Location {
						position:3,
						line: 1
					})]
		);
	}

	#[test]
	fn bad_number()
	{
		let result = lexe("7f");
		assert!(result.is_err());

		let result = lexe("-8f");
		assert!(result.is_err());

		let result = lexe("+9f");
		assert!(result.is_err());
	}

	#[test]
	fn number()
	{
		let mut result = lexe("1");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Number(1),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("+2");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Number(2),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("-3");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Number(-3),
				Location {
					position:1,
					line: 1
				})]
		);
	}

	#[test]
	fn lengthy_number()
	{
		let mut result = lexe("400_000");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Number(400_000),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("+500_000");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Number(500_000),
				Location {
					position:1,
					line: 1
				})]
		);

		result = lexe("-600_000");
		assert!(result.is_ok());
		assert_eq!(
			result.unwrap(),
			vec![(
				TokenType::Number(-600_000),
				Location {
					position:1,
					line: 1
				})]
		);
	}
}
