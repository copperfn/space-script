use crate::location::Location;
use std::cmp::{Eq, PartialEq};
pub type Token = (TokenType, Location);

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum TokenType
{
	Number(i32),
	Word(String),
	SignatureSeparator,
	ListSeparator,
	InfixModifier,
	Assignment,
	NewLine,
}

pub fn is_sign(
	c: char)
	-> bool
{
	match c {
		'+' => true,
		'-' => true,
		_ => false
	}
}
