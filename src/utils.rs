use std::iter::Peekable;
use std::fmt::Debug;

#[derive(Debug)]
pub struct Span<A, I>
where I: Iterator + Debug,
<I as Iterator>::Item: Debug
{
	pub validated: Vec<A>,
	pub remainder: Peekable<I>,
}

pub fn span<A, I, P>(
	p: P,
	mut iter: Peekable<I>
) -> Span<A, I>
where
	A: Clone + Debug,
	I: Iterator<Item = A> + Debug,
	P: Fn(&<I as Iterator>::Item) -> bool
{
	let mut validated: Vec<A> = vec![];

	while let Some(item) = iter.peek() {
		if !p(&item) {
			break;
		}
		validated.push(item.clone());
		iter.next();
	}
	Span {
		validated,
		remainder: iter
	}
}

pub fn clone<A>(item: &A) -> A
where A: Clone
{
	item.clone()
}

#[cfg(test)]
mod test
{
	use super::span;
	use std::vec::Vec;
	#[test]
	fn working()
	{
		let vec: Vec<i32> = vec![1, 2, 3, 4];
		let span = span(|i| *i < 3, vec.into_iter().peekable());

		assert_eq!(vec![1, 2], span.validated);
		assert_eq!(vec![3, 4], span.remainder.collect::<Vec<i32>>());
	}

	#[test]
	fn empty()
	{
		let vec: Vec<i32> = Vec::new();
		let span = span(|i| *i < 3, vec.into_iter().peekable());

		assert_eq!(Vec::new() as Vec<i32>, span.validated);
		assert_eq!(Vec::new() as Vec<i32>, span.remainder.collect::<Vec<i32>>());
	}

	#[test]
	fn all_false()
	{
		let vec: Vec<i32> = vec![1, 2, 3, 4];
		let span = span(|i| *i > 5, vec.into_iter().peekable());

		assert_eq!(Vec::new() as Vec<i32>, span.validated);
		assert_eq!(vec![1, 2, 3, 4], span.remainder.collect::<Vec<i32>>());
	}

	#[test]
	fn all_true()
	{
		let vec: Vec<i32> = vec![1, 2, 3, 4];
		let span = span(|i| *i > 0, vec.into_iter().peekable());

		assert_eq!(vec![1, 2, 3, 4], span.validated);
		assert_eq!(vec![] as Vec<i32>, span.remainder.collect::<Vec<i32>>());
	}
}
