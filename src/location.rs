
use std::{cmp::{Eq, PartialEq}, fmt};

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Location
{
	pub position: i32,
	pub line: i32
}

impl fmt::Display for Location
{
	fn fmt(
		&self,
		f: &mut fmt::Formatter<'_>
	) -> fmt::Result
	{
		write!(f, "at line {}, word {})", self.line, self.position)
	}
}
