use std::hash::{Hash, Hasher};
use std::iter::Peekable;
use std::vec::Vec;
use std::collections::HashMap;

use super::context::{ParsingContextDefinitions, ContextDefinition as UnparsedDefinition, Context};
use super::context::{ParsingContext, Overloads, default_context::default_context};
use crate::ast::context::default_symbole_table::default_symbole_table;
use crate::lexer::token::{Token, TokenType};
use crate::utils::{span, clone};
use crate::location::Location;

pub type Arguments = Vec<Expression>;
pub type Ast = HashMap<String, Overloads>;

#[derive(Debug, Clone, Eq)]
pub struct  Identifier
{
	pub raw: [String; 10],
	pub string: String,
	pub location: Option<Location>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BuiltIn
{
	AddBinOpInt
}

impl ToString for BuiltIn
{
	fn to_string(
		&self
	) -> String {
		match self
		{
			Self::AddBinOpInt => "+".to_string()
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Definition
{
	BuiltIn(BuiltIn),
	Definition(FunctionDefinition),
	FirstClass(Constant),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FunctionDefinition
{
	pub identifier: Identifier,
	pub args: Vec<Identifier>,
	pub body: Expression,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Expression
{
	Constant(Constant),
	FunctionCall(FunctionCall),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Constant
{
	Int(Value<i32>, Location),
	Identifier(Identifier),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Value<T>(pub T);

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FunctionCall
{
	pub identifier: Identifier,
	pub args: Arguments,
}

#[derive(Debug, PartialEq, Eq)]
pub enum AstError
{
	NoMainFound,
	NameToLong(Location),
	NoAssignment(Location),
	NoIdentifierGiven(Location),
	NotAWord(Location),
	AlreadyExist(Identifier),
	NoExpression(Location),
	ExpectedFunctionCallInfix(Location),
	NotAValidExpretion(Token),
	NotFound(Identifier),
	NotFoundOverload(Identifier, String),
	InvalidTokenInFunctionCall(Token),
	Unknown(Location),
}

impl PartialEq for Identifier
{
	// Required method
	fn eq(
		&self,
		other: &Identifier
	)-> bool
	{
		self.raw == other.raw && self.string == other.string
	}
}

impl Hash for Identifier
{
	fn hash<H: Hasher>(
		&self,
		state: &mut H
	)
	{
		self.string.hash(state);
	}
}

impl From<&Vec<&str>> for Identifier
{
	fn from(
		values: &Vec<&str>
	) -> Self
	{
		let mut raw: [String; 10] = [
			String::new(), String::new(), String::new(),
			String::new(), String::new(), String::new(),
			String::new(), String::new(), String::new(),
			String::new()
		];
		let mut idx: usize = 0;

		for identifier in values
			.iter()
			.take(10)
			.map(|_str| _str.to_string())
		{
			raw[idx] = identifier;
			idx += 1;
		}
		Identifier {
			string: raw
				.iter()
				.filter(|word| word.len() != 0)
				.map(|word| word.clone())
				.collect::<Vec<String>>()
				.join(" "),
			raw,
			location: None
		}
	}
}

impl From<Vec<&String>> for Identifier
{
	fn from(
		values: Vec<&String>
	) -> Self
	{
		let mut raw: [String; 10] = [
			String::new(), String::new(), String::new(),
			String::new(), String::new(), String::new(),
			String::new(), String::new(), String::new(),
			String::new()
		];
		let mut idx: usize = 0;

		for identifier in values
			.iter()
			.take(10)
			.map(|_str| _str.to_string())
		{
			raw[idx] = identifier;
			idx += 1;
		}
		Identifier {
			string: raw
				.iter()
				.filter(|word| word.len() != 0)
				.map(|word| word.clone())
				.collect::<Vec<String>>()
				.join(" "),
			raw,
			location: None
		}
	}
}

impl TryFrom<&Vec<Token>> for Identifier
{
	type Error = AstError;

	fn try_from(
		tokens: &Vec<Token>
	) -> Result<Self, AstError>
	{
		let mut raw: [String; 10] = [
			String::new(), String::new(), String::new(),
			String::new(), String::new(), String::new(),
			String::new(), String::new(), String::new(),
			String::new()
		];
		let mut idx: usize = 0;
		let location = tokens[0]
			.1
			.clone();

		for identifier_token in tokens
			.iter()
			.take(10)
		{
			match identifier_token
			{
				(TokenType::Word(identifier), _) => raw[idx] = identifier.clone(),
				(_, location) => return Err(AstError::NotAWord(location.clone()))
			}
			idx += 1;
		}
		Ok(
			Identifier {
			string: raw
				.iter()
				.filter(|word| word.len() != 0)
				.map(|word| word.clone())
				.collect::<Vec<String>>()
				.join(" "),
			raw,
			location: Some(location)
		})
	}
}

pub fn ast(
	token_stream: Vec<Token>
) -> Result<Ast, AstError>
{
	let context = self::context::get_context(token_stream)?;
	let context = Context::new(context);

	let mut ast = default_symbole_table();

	let main_identifier = Identifier::from(&vec!["main"]);
	let main = context
		.get(&main_identifier.raw)
		.and_then(|overloads| overloads.get(&"()".to_string()));

	if main.is_none() {
		return Err(AstError::NoMainFound)
	}
	let main = main.unwrap();

	match main
	{
		ParsingContextDefinitions::Defined(main) => {
			parse_function(
				main,
				&context,
				&mut ast
			)?;
		},
		_ => panic!("Main should never be implemented as a build in!")
	}

	return Ok(ast)
}

fn parse_function(
	definition: &UnparsedDefinition,
	context: &Context<ParsingContext>,
	ast: &mut Ast,
) -> Result<(), AstError>
{
	if ast.contains_key(&definition.identifier.string)
	{
		return parse_overload(
			definition,
			context,
			ast
				.get_mut(&definition.identifier.string)
				.unwrap()
		)
	}
	let mut overloads = Vec::new();
	let (parsed_definition, overload_key) = parse_function_definitions(
		&definition,
		context,
		ast
	)?;
	overloads.push(
		parsed_definition
	);
	ast.insert(
		definition
			.identifier
			.string
			.clone(),
		overloads
	);
	Ok(())
}

fn parse_function_definitions(
	definition: &UnparsedDefinition,
	context: &Context<ParsingContext>,
	ast: &mut Ast
) -> Result<(Definition, String), AstError>
{
	let local_context: Context<ParsingContext> = definition
		.args
		.iter()
		.fold(
			context.clear(),
			|mut local_context, identifier|
			{
				let mut hash = HashMap::new();
				hash.insert(
					"()".to_string(),
					ParsingContextDefinitions::Arg(identifier.clone())
				);

				local_context
				.insert(
					&identifier.raw,
					hash);
				local_context
			});
	let body = parse_expression(
		&definition.body,
		local_context,
		&definition
			.identifier
			.location,
		ast,
	)?;
	let type_: String = if definition.args.len() == 0
	{
		"()".to_string()
	} else
	{
		format!(
			"({})",
			definition
				.args
				.len()
		)
	};
	let function = Definition::Definition(
		FunctionDefinition {
			identifier: definition
				.identifier
				.clone(),
			args: definition
				.args
				.iter()
				.map(|arg| arg.clone())
				.collect(),
			body,
		});
	Ok((function, type_))
}

fn parse_expression(
	body: &Vec<Token>,
	context: Context<ParsingContext>,
	location: &Option<Location>,
	ast: &mut Ast,
) -> Result<Expression, AstError>
{
	let mut body = span(
		|(token, _)| *token != TokenType::InfixModifier,
		body.into_iter().peekable()
	);
	if body.validated.len() == 0
	{
		let next = body.remainder.next();

		return if let Some((_, location)) = next
		{
			Err(
				AstError::NoExpression(location.clone())
			)
		} else
		{
			Err(
				AstError::NoExpression(
					location
						.clone()
						.expect("Every token should have a location")
				)
			)
		}
	}
	let expression = match body.validated[0]
	{
		(TokenType::Number(n), location) =>
			Expression::Constant(
				Constant::Int(
					Value(n.clone()),
					location.clone()
				)),
		(TokenType::Word(_), _) =>
		{
			let function_call = parse_function_call(
				body.validated,
				context.clone(),
				ast,
			)?;
			if function_call.args.len() == 0
			{
				Expression::Constant(
					Constant::Identifier(function_call.identifier)
				)
			} else {
				Expression::FunctionCall(function_call)
			}
		},
		token => panic!(
			"Not an expretion{:?}",
			token.clone()
		),
	};
	let next = body
		.remainder
		.next();

	if next.is_some()
	{
		return build_infix_expression(
			expression,
			&body.remainder.cloned().collect(),
			context,
			next.unwrap().1.clone(),
			ast,
		)
	}
	Ok(expression)
}

fn build_infix_expression(
	left: Expression,
	body: &Vec<Token>,
	context: Context<ParsingContext>,
	location: Location,
	ast: &mut Ast,
) -> Result<Expression, AstError>
{
	let mut body = span(
		|(token, _)| *token != TokenType::InfixModifier,
		body.into_iter().peekable()
	);
	if body.validated.len() == 0
	{
		return Err(
			AstError::NoExpression(location)
		);
	}
	let mut function_call = match body.validated[0]
	{
		(TokenType::Word(_), _) =>
			parse_function_call(
				body.validated,
				context.clone(),
				ast,
			)?,
		_ => return Err(AstError::ExpectedFunctionCallInfix(location)),
	};
	function_call.args.insert(0, left);
	let expression = Expression::FunctionCall(function_call);
	let next = body
		.remainder
		.next();
	if next.is_some()
	{
		return build_infix_expression(
			expression,
			&body.remainder.cloned().collect(),
			context,
			next.unwrap().1.clone(),
			ast,
		)
	}
	Ok(expression)
}

fn parse_function_call(
	body: Vec<&Token>,
	context: Context<ParsingContext>,
	ast: &mut Ast,
) -> Result<FunctionCall, AstError>
{
	let location = body[0]
			.1
		.clone();
	let mut body_iter = body
		.into_iter()
		.cloned()
		.peekable();
	let function_identifier = match_longest(
		&mut body_iter,
		&context,
		location.clone()
	);

	if function_identifier.is_none()
	{
		return Err(AstError::Unknown(location));
	}
	let function_identifier = function_identifier.unwrap();

	let args = get_args(
		body_iter,
		vec![],
		&context,
	)?;
	for arg in args
		.iter()
		.filter(
			|arg| match *arg
			{
				Expression::Constant(Constant::Identifier(_)) => true,
				_ => false,
			})
		.map(
			|function| match function
			{
				Expression::Constant(Constant::Identifier(id)) => id,
				_ => panic!()
			})
	{
		if context.get(&arg.raw).is_none() ||
			ast.contains_key(&arg.string)
		{
			continue;
		}

		let definition = context.get(&arg.raw)
			.ok_or(AstError::NotFound(arg.clone()))?
			.get(&"()".to_string())
			.ok_or(AstError::NotFoundOverload(
				arg.clone(),
				"()".to_string()
			))?;
		if let ParsingContextDefinitions::Defined(definition) = definition
		{
			parse_function(
				definition,
				&context,
				ast
			)?;
		}
	}

	if !ast.contains_key(&function_identifier.string)
	{
		let definition = context.get(&function_identifier.raw)
			.ok_or(AstError::NotFound(function_identifier.clone()))?
			.get(&"()".to_string())
			.ok_or(AstError::NotFoundOverload(
				function_identifier.clone(),
				"()".to_string()
			))?;
		if let ParsingContextDefinitions::Defined(definition) = definition
		{
			parse_function(
				definition,
				&context,
				ast
			)?;
		}
	}
	Ok(FunctionCall {
		identifier: function_identifier,
		args
	})
}

fn get_args<I>(
	mut iter: Peekable<I>,
	mut args: Arguments,
	context: &Context<ParsingContext>,
) -> Result<Arguments, AstError>
	where I: Iterator<Item = Token> + Clone + std::fmt::Debug
{
	let token = iter.peek().cloned();
	if token.is_none()
	{
		return Ok(args);
	}
	let token = token.as_ref().unwrap();

	match token
	{
		(TokenType::NewLine, _) => return Ok(args),
		(TokenType::Number(n), location) => {
			args.push(
				Expression::Constant(
					Constant::Int(
						Value(n.clone()),
						location.clone()
					)));
			iter.next();
			get_args(
				iter,
				args,
				context,
			)
		},
		(TokenType::Word(_), _) =>
		{
			let matched = match_longest(
				&mut iter,
				context,
				token.1.clone()
			);
			if matched.is_none()
			{
				return Ok(args)
			}
			args.push(
				Expression::Constant(
					Constant::Identifier(matched.unwrap())
				));
			get_args(
				iter,
				args,
				context,
			)
		}
		token => return Err(AstError::InvalidTokenInFunctionCall((*token).clone()))
	}
}

fn match_longest<I>(
	iter: &mut Peekable<I>,
	context: &Context<ParsingContext>,
	location: Location,
) -> Option<Identifier>
where I: Iterator<Item = Token>,
{
	let mut identifier_raw: [String; 10] = [
		String::new(), String::new(), String::new(), String::new(),
		String::new(), String::new(), String::new(),
		String::new(), String::new(), String::new()
	];
	let mut identifier_index = 0;
	let mut sub_trie = Some(&**context);
	let mut token = iter.peek().cloned();

	loop
	{
		let word = if let Some((TokenType::Word(ref word), _)) = token
		{
			word
		} else
		{
			break
		};

		sub_trie = sub_trie
			.unwrap()
			.get_node(&[word.clone()]);
		if sub_trie.is_none()
		{
			break
		}
		identifier_raw[identifier_index] = word.clone();
		if identifier_index > 10 ||
			iter.peek().is_none()
		{
			break;
		}
		iter.next();
		token = iter.peek().cloned();
		identifier_index += 1;
	}
	let identifier = Identifier {
		location: Some(location),
		string: identifier_raw
			.iter()
			.take_while(|word| word.len() > 0)
			.map(clone)
			.collect::<Vec<String>>()
			.join(" "),
		raw: identifier_raw
	};
	if identifier_index == 0
	{
		return None
	}
	return Some(identifier)
}

fn parse_overload(
	_definition: &UnparsedDefinition,
	_context: &ParsingContext,
	_overloads: &mut Overloads,
) -> Result<(), AstError>
{
	todo!()
}

mod context
{
	use std::fmt::Debug;

use crate::ast::context::ContextDefinition;
	use super::*;

	pub fn get_context(
		token_stream: Vec<Token>
	) -> Result<ParsingContext, AstError>
	{
		let mut context = default_context();
		let mut line = span(
			|token| token.0 != TokenType::NewLine,
			token_stream.into_iter().peekable()
		);
		loop
		{
			if line.validated.len() == 0
			{
				line = span(
					|token: &Token| token.0 != TokenType::NewLine,
					line.remainder
				);
				line.remainder.next();
				continue;
			}
			let mut assignment = span(
				|token| {
					token.0 != TokenType::NewLine    &&
					token.0 != TokenType::Assignment &&
					token.0 != TokenType::SignatureSeparator
				},
				line
					.validated
					.iter()
					.peekable()
			);
			match assignment.remainder.next()
			{
				Some((TokenType::Assignment, location)) => add_global_context(
					assignment
						.validated
						.into_iter()
						.cloned()
						.collect(),
					location,
					assignment
						.remainder
						.cloned()
						.collect(),
					&mut context
				)?,
				Some((TokenType::SignatureSeparator, location)) => add_function_context(
					assignment
						.validated
						.into_iter()
						.cloned()
						.collect(),
					location,
					assignment
						.remainder
						.cloned()
						.peekable(),
					&mut context,
				)?,
				_ => return Err(
					AstError::NoAssignment(
						line
							.validated[0]
							.1
							.clone()
					)),
			}
			if line
				.remainder
				.next()
				.is_none()
			{
				return Ok(context);
			}
			line = span(
				|token: &Token| token.0 != TokenType::NewLine,
				line.remainder
			);
		}
	}

	fn add_global_context(
		name: Vec<Token>,
		assignment_location: &Location,
		body: Vec<Token>,
		current_context: &mut ParsingContext,
	) -> Result<(), AstError>
	{
		if name.len() == 0
		{
			return Err(
				AstError::NoIdentifierGiven(assignment_location.clone())
			);
		}
		if name.len() > 10
		{
			return Err(
				AstError::NameToLong(assignment_location.clone())
			);
		}
		let identifier = Identifier::try_from(&name)?;
		let overloads = current_context.get_mut(&identifier.raw);
		let definition = ContextDefinition {
			identifier: identifier.clone(),
			body,
			args: Vec::new()
		};

		let overload_key = if definition.args.len() == 0
			{"()".to_string()} else {format!("({})", definition.args.len())};
		if overloads.is_none()
		{
			let mut overloads = HashMap::new();
			overloads.insert(
				overload_key,
				ParsingContextDefinitions::Defined(definition)
			);
			current_context.insert(
				&identifier.raw,
				overloads
			);
			return Ok(())
		}
		let overloads = overloads.unwrap();
		if overloads.contains_key(&overload_key)
		{
			return Err(
				AstError::AlreadyExist(definition.identifier)
			)
		}
		overloads.insert(
			overload_key,
			ParsingContextDefinitions::Defined(definition)
		);
		Ok(())
	}

	fn add_function_context<I>(
		name: Vec<Token>,
		function_location: &Location,
		remainder: Peekable<I>,
		current_context: &mut ParsingContext,
	) -> Result<(), AstError>
	where I: Iterator<Item = Token> + Debug + Clone,
	{
		if name.len() == 0
		{
			return Err(
				AstError::NoIdentifierGiven(function_location.clone())
			);
		}
		if name.len() > 10
		{
			return Err(
				AstError::NameToLong(function_location.clone())
			);
		}
		let identifier = Identifier::try_from(&name)?;
		let overloads = current_context.get_mut(&identifier.raw);
		let mut assignment = span(
			|token| token.0 != TokenType::Assignment,
			remainder,
		);
		let stopped = assignment.remainder.next();
		if stopped.is_none() ||
			stopped.unwrap().0 != TokenType::Assignment
		{
			return Err(
				AstError::NoAssignment(function_location.clone())
			);
		}
		let body = assignment.remainder.collect();
		let definition = ContextDefinition {
			identifier: identifier.clone(),
			body,
			args: collect_args_identifier(
				assignment.validated,
				function_location.clone()
			)?,
		};
		if overloads.is_none()
		{
			let mut overloads = HashMap::new();
			overloads.insert(
				"()".to_string(),
				ParsingContextDefinitions::Defined(definition)
			);
			current_context.insert(
				&identifier.raw,
				overloads
			);
			return Ok(())
		}
		let overloads = overloads.unwrap();
		if overloads.contains_key("()")
		{
			return Err(
				AstError::AlreadyExist(definition.identifier)
			)
		}
		overloads.insert(
			"".to_string(),
			ParsingContextDefinitions::Defined(definition)
		);
		Ok(())
	}
}

fn collect_args_identifier(
	tokens: Vec<Token>,
	location: Location,
) -> Result<Vec<Identifier>, AstError>
{
	let mut args = Vec::new();
	let mut identifier_token = span(
		|token| token.0 != TokenType::ListSeparator,
		tokens.into_iter().peekable()
	);
	let mut location = location;
	while let Some((TokenType::ListSeparator, new_location)) = identifier_token
		.remainder
		.next()
	{
		if identifier_token.validated.len() == 0
		{
			return Err(AstError::NoIdentifierGiven(location))
		}
		location = new_location;
		let identifier = Identifier::try_from(&identifier_token.validated)?;
		args.push(identifier);
		identifier_token = span(
			|token| token.0 != TokenType::ListSeparator,
			identifier_token.remainder
		);
	}
	if identifier_token.validated.len() == 0
	{
		return Err(AstError::NoIdentifierGiven(location))
	}
	let identifier = Identifier::try_from(&identifier_token.validated)?;
	args.push(identifier);
	Ok(args)
}

#[cfg(test)]
mod test
{
	use crate::lexer::lexer::lexe;
	use crate::ast::context::default_symbole_table::default_symbole_table;
	use super::*;

	fn run(
		input: &str
	) -> Result<Ast, AstError>
	{
		ast(lexe(input).unwrap())
	}

	#[test]
	fn simple_main()
	{
		let mut reference = default_symbole_table();
		let mut main = Vec::new();
		let result = run("main = 1");

		let mut main_id = Identifier::from(&vec!["main"]);
		main_id.location = Some(
			Location {
				position: 1,
				line: 1
			});
		main.push(
			Definition::Definition(FunctionDefinition {
				identifier: main_id,
				body: Expression::Constant(
					Constant::Int(
						Value(1),
						Location { position: 8, line: 1 },
						)),
				args: vec![],
			}));

		reference.insert(
			"main".to_string(),
			main
		);
		assert!(result.is_ok());
		assert_eq!(
			reference,
			result.unwrap()
		);
	}

	#[test]
	fn no_main()
	{
		let result = run("my = 3");

		assert_eq!(
			result,
			Err(AstError::NoMainFound)
		)
	}

	#[test]
	fn no_assignment()
	{
		let result = run("main 3");

		assert_eq!(
			result,
			Err(
				AstError::NoAssignment(
					Location {
						position: 1,
						line: 1
					})))
	}

	#[test]
	fn no_body()
	{
		let result = run("main = ");

		let mut main_id = Identifier::from(&vec!["main"]);
		main_id.location = Some(
			Location {
				position: 1,
				line: 1
			}
		);
		assert_eq!(
			result,
			Err(
				AstError::NoExpression(main_id.location.unwrap())
			))
	}

	#[test]
	fn name_to_long()
	{
		let result = run("zero one two Tree four five six seven heigt nine ten = 0
main = 1");

		assert_eq!(
			result,
			Err(
				AstError::NameToLong(
					Location { position: 54, line: 1 }
				)))
	}

	#[test]
	fn no_name()
	{
		let result = run(" = 0
main = 1");

		assert_eq!(
			result,
			Err(
				AstError::NoIdentifierGiven(
					Location {
						position: 2,
						line: 1
					})))
	}

	#[test]
	fn not_a_word()
	{
		let result = run("3 = 3
main = 1");

		assert_eq!(
			result,
			Err(
				AstError::NotAWord(
					Location {
						position: 1,
						line: 1
					})))
	}

	#[test]
	fn already_exist()
	{
		let result = run("one = 1

one = 2
main = 1");

		let mut one_id = Identifier::from(&vec!["one"]);
		one_id.location = Some(
			Location {
				position: 1,
				line: 3
			});
		assert_eq!(
			result,
			Err(
				AstError::AlreadyExist(one_id)
			))
	}

		#[test]
	fn infix_function_call_err()
	{

		let result = run("my add: a, b = .+ b
main = 5 .my add 5");
		assert!(result.is_err());
		assert_eq!(
			result,
			Err(
				AstError::NoExpression(
					Location {
						position: 16,
						line: 1
					}))
		);

		let result = run("my add:a,b=a.
main=5.my add 5");
		assert!(result.is_err());
		assert_eq!(
			result,
			Err(
				AstError::NoExpression(
					Location {
						position: 13,
						line: 1
					}))
		);
	}
}
