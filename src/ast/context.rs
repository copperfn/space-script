
use std::{collections::HashMap, fmt::Debug, ops::{Deref, DerefMut}};

use crate::lexer::token::Token;
use super::ast::{Identifier, Definition};
use sequence_trie::SequenceTrie;

#[derive(Debug, Clone)]
pub struct Context<T>
	where T: Debug + Clone
{
	global: T,
	pub local: T,
}

impl<T: Debug + Clone> Context<T>
{
	pub fn new(global: T) -> Context<T>
	{
		Context {
			local: global.clone(),
			global,
		}
	}

	pub fn clear(&self) -> Self
	{
		let mut new_context = self.clone();
		new_context.local = new_context.global.clone();
		new_context
	}
}

impl<T: Debug + Clone> Deref for Context<T>
{
	type Target = T;

	fn deref(&self) -> &Self::Target {
		&self.local
	}
}

impl<T: Debug + Clone> DerefMut for Context<T>
{
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.local
	}
}

#[derive(Debug, Clone)]
pub enum ParsingContextDefinitions
{
	Defined(ContextDefinition),
	Arg(Identifier),
	BuildIn,
}

#[derive(Debug, Clone)]
pub struct ContextDefinition
{
	pub identifier: Identifier,
	pub args: Vec<Identifier>,
	pub body: Vec<Token>
}

pub type ParsingContext = SequenceTrie<String, HashMap<String, ParsingContextDefinitions>>;
pub type Overloads = Vec<Definition>;

pub mod default_context
{
	use crate::ast::ast::BuiltIn;

	use super::*;
	use std::collections::HashMap;
	pub fn default_context(
	) -> ParsingContext
	{
		let mut context = SequenceTrie::new();

		context.insert(
			&Identifier::from(&vec![BuiltIn::AddBinOpInt.to_string().as_str()]).raw,
			add_overloads()
		);
		return context
	}

	fn add_overloads(
	) -> HashMap<String, ParsingContextDefinitions>
	{
		let mut overloads = HashMap::new();

		overloads.insert(
			"(2)".to_string(),
			ParsingContextDefinitions::BuildIn);
		overloads
	}
}

pub mod default_symbole_table
{
	use crate::ast::ast::{Ast, BuiltIn};
	use super::*;
	use std::collections::HashMap;

	pub fn default_symbole_table(
	) -> Ast
	{
		let mut symbole_table = HashMap::new();

		symbole_table.insert(
			BuiltIn::AddBinOpInt.to_string(),
			add_overloads());

		symbole_table
	}

	fn add_overloads(
	) -> Overloads
	{

		vec![Definition::BuiltIn(BuiltIn::AddBinOpInt)]
	}
}
