
mod lexer;
mod location;
mod ast;
mod utils;

mod type_checker;

use crate::lexer::lexer::lexe;
use crate::ast::ast::ast;

#[cfg(not(tarpaulin_include))]
fn main()
{
	use crate::ast::context::Context;

	let ast = ast(lexe("main = 1 .+ 2 .add 5
add: a, b = + a b"
	).unwrap()).unwrap();
	println!(
		"\n{:#?}\n",
		type_checker::type_infrerence::get_typed_ast(Context::new(ast))
	);
}
