use std::
{
	collections::
	{
		HashMap,
		HashSet,
		VecDeque,
	},
	hash::
	{
		Hash,
		Hasher,
	},
	slice::Iter,
	fmt::
	{
		Display,
		Formatter,
		Error,
	}
};

use sequence_trie::SequenceTrie;
use crate::ast::
{
	ast::
	{
		Identifier,
		Definition,
		Ast,
		Constant,
		FunctionDefinition,
		Expression,
		FunctionCall,
		BuiltIn,
	},
	context::
	{
		Context,
		Overloads,
	}
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TypedDefinition
{
	BuiltIn(BuiltIn, Type),
	Definition(FunctionDefinition, Type),
	FirstClass(Constant, Type),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TypeFunctionDefinition
{
	pub identifier: Identifier,
	pub args: Vec<Identifier>,
	pub body: TypedExpression,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TypedExpression
{
	Constant(Constant, Type),
	FunctionCall(TypedFunctionCall),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TypedFunctionCall
{
	pub identifier: Identifier,
	pub args: Vec<(Expression, Type)>,
}

pub type TypedOverloads = SequenceTrie<Type, TypedDefinition>;
pub type TypedAst = HashMap<String, TypedOverloads>;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TypeErr
{
	TypeMissmatch,
	MultipleDefinitions,
	MissMatchNumberOfArgument,
	UnusedParameter(Identifier),
	MultipleTypeForArgument(Vec<Type>, Identifier)
}

#[derive(Debug, Clone)]
pub struct TypeIter<'a>
{
	iter: Option<Iter<'a, Type>>,
	return_type: &'a Type,
	end: bool,
}

impl<'a> Iterator for TypeIter<'a>
{
	type Item = &'a Type;
	fn next(
		&mut self
	) -> Option<Self::Item>
	{
		if self.end
		{
			return None
		}
		match &mut self.iter
		{
			None => {
				self.end = true;
				return Some(self.return_type)
			}
			Some(iter) =>
			{
				let next = iter.next();
				if next.is_some()
				{
					return next;
				}
				self.end = true;
				return Some(self.return_type)
			}
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Type
{
	Type(Identifier),
	Generic(Identifier),
	Signature(Vec<Type>, Box<Type>),
}

impl Type
{
	fn is_generic(
		&self
	) -> bool
	{
		match self
		{
			Self::Generic(_) => true,
			Self::Signature(args, return_type) =>
			{
				let generic_in_args = args.iter().any(|arg| arg.is_generic());
				generic_in_args || return_type.is_generic()
			}
			_ => false,
		}
	}
}

impl Display for Type
{
	fn fmt(
		&self,
		f: &mut Formatter<'_>
	) -> Result<(), Error>
	{
		match self
		{
			Type::Signature(args, return_type) =>
			{
				let args:String = args
					.iter()
					.map(|id| format!("{}", id))
					.collect::<Vec<String>>()
					.join("->");
				write!(f, "({}->{})", args, *return_type)?
			},
			Type::Type(t) => write!(f, "{}", t.string)?,
			Type::Generic(t) => write!(f, "{}", t.string)?,
		}
		Ok(())
	}

}

impl<'a> IntoIterator for &'a Type
{
	type Item = &'a Type;
	type IntoIter = TypeIter<'a>;
	fn into_iter(
		self
	) -> Self::IntoIter
	{
		match self
		{
			t@Type::Type(_) => TypeIter {
				iter: None,
				return_type: t,
				end: false,
			},
			t@Type::Generic(_) => TypeIter {
				iter: None,
				return_type: t,
				end: false,
			},
			Type::Signature(args, return_type) => TypeIter {
				iter: Some(args.iter()),
				return_type: &*return_type,
				end: false,
			}
		}
	}
}

impl From<Vec<Type>> for Type
{
	fn from(
		vec: Vec<Type>,
	) -> Type
	{
		if vec.len() == 0
		{
			panic!()
		} else if vec.len() == 1
		{
			return vec[0].clone()
		}
		let mut args: Vec<Type> = vec
			.into_iter()
			.collect();
		let return_type = args
			.pop()
			.unwrap();
		return Type::Signature(
			args,
			Box::new(return_type)
		);
	}
}

impl From<Vec<&Type>> for Type
{
	fn from(
		vec: Vec<&Type>,
	) -> Type
	{
		if vec.len() == 0
		{
			panic!()
		} else if vec.len() == 1
		{
			return vec[0].clone()
		}
		let mut args: Vec<Type> = vec
			.into_iter()
			.cloned()
			.collect();
		let return_type = args
			.pop()
			.unwrap()
			.clone();
		return Type::Signature(
			args,
			Box::new(return_type)
		);
	}
}

impl Hash for Type
{
	fn hash<H: Hasher>(
		&self,
		state: &mut H
	)
	{
		match self
		{
			Type::Signature(signature, return_type) =>
			{
				signature.hash(state);
				return_type.hash(state);
			},
			Type::Type(type_) => type_.hash(state),
			Type::Generic(type_) => type_.hash(state),
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum AmbiguousType
{
	NotAmbiguous(Type),
	Ambiguous(Vec<Type>)
}

impl Display for AmbiguousType
{
	fn fmt(
		&self,
		f: &mut Formatter<'_>
	) -> Result<(), Error>
	{
		match self
		{
			AmbiguousType::Ambiguous(types) =>
			{
				let types:String = types
					.iter()
					.map(|id| format!("{}", id))
					.collect::<Vec<String>>()
					.join(", ");
				write!(f, "[{}]", types)
			},
			AmbiguousType::NotAmbiguous(type_) =>
			{
				write!(f, "[{}]", type_)
			}
		}
	}
}

impl From<AmbiguousType> for HashSet<Type>
{
	fn from(
		type_: AmbiguousType,
	) -> HashSet<Type>
	{
		match type_
		{
			AmbiguousType::NotAmbiguous(type_) =>
			{
				let mut hash_set = HashSet::with_capacity(1);
				hash_set.insert(type_);
				hash_set
			}
			AmbiguousType::Ambiguous(type_vec) =>
			{
				type_vec
					.iter()
					.cloned()
					.fold(
						HashSet::with_capacity(type_vec.capacity()),
						|mut hash_set, type_|
						{
							hash_set.insert(type_);
							hash_set
						})
			}
		}
	}
}

impl From<Vec<Type>> for AmbiguousType
{
	fn from(
		vec: Vec<Type>,
	) -> AmbiguousType
	{
		if vec.len() == 0
		{
			panic!()
		} else if vec.len() == 1
		{
			return AmbiguousType::NotAmbiguous(vec[0].clone())
		}
		return AmbiguousType::Ambiguous(
			vec,
		);
	}
}

impl From<&AmbiguousType> for Vec<Type>
{
	fn from(
		type_: &AmbiguousType,
	) -> Vec<Type>
	{
		match type_
		{
			AmbiguousType::Ambiguous(vec) => vec.clone(),
			AmbiguousType::NotAmbiguous(type_) => vec![type_.clone()]
		}
	}
}

pub fn get_typed_ast(
	context: Context<Ast>
) -> Result<TypedAst, TypeErr>
{
	let main_overload = (*context)
		.get("main")
		.unwrap();

	if main_overload.len() != 1
	{
		panic!()
	}
	let type_ast = default_typed_ast();

	infer(
		main_overload,
		&context,
		type_ast,
		&"main".to_string(),
	)
}

fn get_definition_type(
	definition: &Definition,
	context: &Context<Ast>,
	typed_ast: TypedAst,
) -> Result<(Vec<TypedDefinition>, TypedAst), TypeErr>
{
	match definition
	{
		Definition::BuiltIn(built_in) =>
		{
			let type_ = built_in_type(built_in);
			Ok((
				vec![TypedDefinition::BuiltIn(built_in.clone(), type_)],
			typed_ast
			))
		},
		Definition::FirstClass(constant) =>
		{
			let (types, new_ast) = constant_type(
				constant,
				context,
				typed_ast,
			)?;
			match types
			{
				AmbiguousType::NotAmbiguous(type_) =>
				{
					Ok((
						vec![TypedDefinition::FirstClass(constant.clone(), type_)],
						new_ast
					))
				},
				AmbiguousType::Ambiguous(types) =>
				{
					let typed_definitions: Vec<TypedDefinition> = types
						.into_iter()
						.map(|type_| TypedDefinition::FirstClass(
							constant.clone(),
							type_))
						.collect();
					Ok((
						typed_definitions,
						new_ast
					))
				}
			}
		},
		Definition::Definition(function_definition) =>
		{
			let (types, new_ast) = get_function_definition_type(
				function_definition,
				context,
				typed_ast,
			)?;
			match types
			{
				AmbiguousType::NotAmbiguous(type_) =>
				{
					Ok((
						vec![TypedDefinition::Definition(
							function_definition.clone(),
							type_
						)],
						new_ast
					))
				},
				AmbiguousType::Ambiguous(types) =>
				{
					let typed_definitions: Vec<TypedDefinition> = types
						.into_iter()
						.map(|type_| TypedDefinition::Definition(
							function_definition.clone(),
							type_
						))
						.collect();
					Ok((
						typed_definitions,
						new_ast
					))
				}
			}
		}
	}
}

fn get_function_definition_type(
	function_definition: &FunctionDefinition,
	context: &Context<Ast>,
	typed_ast: TypedAst,
) -> Result<(AmbiguousType, TypedAst), TypeErr>
{
	let mut generics_lookup: HashMap<String, HashSet<Type>> = HashMap::with_capacity(
		function_definition
			.args
			.len()
	);
	let mut definition_types: Vec<Type> = vec![];

	let definition_args_type = ('a'..='z')
		.map(|char_| String::from(char_))
		.zip(function_definition.args.iter())
		.fold(
			HashMap::new(),
			|mut hash, (generic_name, arg)|
			{
				hash.insert(
					arg.clone(),
					generic_name
				);
				hash
			}
		);
	let (_, new_type_ast) = get_expression_type(
		&function_definition.body,
		&definition_args_type,
		&mut generics_lookup,
		function_definition,
		&mut definition_types,
		&context,
		typed_ast,
		true
	)?;
	Ok((AmbiguousType::from(definition_types), new_type_ast))
}

fn get_expression_type(
	expression: &Expression,
	definition_args_names: &HashMap<Identifier, String>,
	generic_lookup: &mut HashMap<String, HashSet<Type>>,
	function_definition: &FunctionDefinition,
	definition_types: &mut Vec<Type>,
	context: &Context<Ast>,
	typed_ast: TypedAst,
	is_last_expression: bool,
) -> Result<(AmbiguousType, TypedAst), TypeErr>
{
	match &expression
	{
		Expression::Constant(constant) =>
		{
			if let Constant::Identifier(id) = constant
			{
				if let Some(generic_name) = definition_args_names.get(id)
				{
					match generic_lookup.get(generic_name)
					{
						Some(types) =>
						{
							let type_vec: Vec<Type> = types
								.iter()
								.cloned()
								.collect();

							return Ok((
								AmbiguousType::from(type_vec),
								typed_ast
							))
						},
						None =>
						{
							let mut type_identifier = Identifier::from(vec![generic_name]);
							type_identifier.location = id.location.clone();
							let type_ = Type::Generic(type_identifier);

							return Ok((
								AmbiguousType::NotAmbiguous(type_),
								typed_ast,
							))
						}
					}
				}
			}
			constant_type(
				constant,
				context,
				typed_ast,
			)
		},
		Expression::FunctionCall(function_call) => check_function_call(
			function_call,
			definition_args_names,
			generic_lookup,
			function_definition,
			definition_types,
			context,
			typed_ast,
			is_last_expression,
		),
	}
}

fn check_function_call(
	function_call: &FunctionCall,
	definition_arg_names: &HashMap<Identifier, String>,
	generic_lookup: &mut HashMap<String, HashSet<Type>>,
	function_definition: &FunctionDefinition,
	definition_type: &mut Vec<Type>,
	context: &Context<Ast>,
	typed_ast: TypedAst,
	is_last_expression: bool,
) -> Result<(AmbiguousType, TypedAst), TypeErr>
{
	let function_name = &function_call
		.identifier
		.string;

	match typed_ast.get(
		function_name
	).cloned()
	{
		Some(overload) =>
		{
			already_infered(
				&overload,
				function_call,
				definition_arg_names,
				function_definition,
				definition_type,
				context,
				typed_ast,
				is_last_expression,
			)
		},
		None => not_infered(
			function_call,
			definition_arg_names,
			generic_lookup,
			function_definition,
			definition_type,
			context,
			typed_ast,
			function_name,
			is_last_expression,
		)
	}
}

fn already_infered(
	overloads: &TypedOverloads,
	function_call: &FunctionCall,
	definition_args_names: &HashMap<Identifier, String>,
	function_definition: &FunctionDefinition,
	definition_types: &mut Vec<Type>,
	context: &Context<Ast>,
	mut typed_ast: TypedAst,
	is_last_expression: bool,
) -> Result<(AmbiguousType, TypedAst), TypeErr>
{
	let mut return_types: Vec<Type> = Vec::new();

	let (possible_signatures, new_typed_ast) = get_signatures(
		overloads,
		function_call.args.iter(),
		definition_args_names,
		HashMap::new(),
		function_definition,
		definition_args_names,
		definition_types,
		context,
		typed_ast,
	)?;
	typed_ast = new_typed_ast;

	match possible_signatures
	{
		None => todo!(),
		Some(signatures) =>
		{

			for (signature, (local_lookup, definition, is_curried)) in signatures.iter()
			{

				let return_type = (*signature).last().unwrap().clone();
				if !is_curried
				{
					let definition_type = build_definition_type(
						definition_args_names,
						local_lookup,
						function_definition,
						return_type.clone()
					);

					definition_types.push(definition_type);

					let function_overload = typed_ast.get_mut(
						&function_call.identifier.string
					).unwrap();
					if function_overload.get(signature.clone()).is_none()
					{
						function_overload
							.insert(
								signature,
								definition.clone()
							);
					}
				}
				return_types.push(return_type.clone());
			}
		}
	}
	return Ok((
		AmbiguousType::from(return_types),
		typed_ast
	));
}

fn get_signatures(
	node: &TypedOverloads,
	mut args: Iter<Expression>,
	args_names: &HashMap<Identifier, String>,
	mut local_lookup: HashMap<String, HashSet<Type>>,
	function_definition: &FunctionDefinition,
	definition_arg_names: &HashMap<Identifier, String>,
	definition_types: &mut Vec<Type>,
	context: &Context<Ast>,
	mut typed_ast: TypedAst,
) -> Result<
		(
			Option<
					SequenceTrie<
						Type,
						(
							HashMap<String, HashSet<Type>>,
							TypedDefinition,
							bool,
						)>
					>,
			TypedAst,
		),
	TypeErr>
{
	let expression = args.next();

	match expression
	{
		None =>
		{
			let return_vec = node
				.iter()
				.map(
					|(type_vec, definition)|
					{
						let is_curried = type_vec.len() != 1;
						(Type::from(type_vec), definition.clone(), is_curried)
					}
				).collect::<Vec<(Type, TypedDefinition, bool)>>();

			if return_vec.len() == 0
			{
				return Ok((None, typed_ast))
			}
			let mut trie_node = SequenceTrie::new();
			for (type_, definition, is_curried) in return_vec.into_iter()
			{
				let return_type = simplify_generic(
					type_,
					&local_lookup,
				);
				trie_node.insert(
					&vec![return_type],
					(local_lookup.clone(), definition, is_curried),
				);
			}
			return Ok(
				(Some(trie_node), typed_ast)
			)
		},
		Some(expression) =>
		{
			let (types, new_ast) = get_expression_type(
				expression,
				args_names,
				&mut local_lookup,
				function_definition,
				definition_types,
				context,
				typed_ast,
				false
			)?;
			typed_ast = new_ast;

			let expression_type_vec = <&AmbiguousType as Into<Vec<Type>>>::into(&types);
			let mut trie_node = SequenceTrie::new();
			for expression_type in expression_type_vec
				.iter()
			{
				let iter = node.children_with_keys()
					.into_iter()
					.filter(
						|(key, _)|
						{
							type_match(key, expression_type)
						}
					);
				for (function_type, child) in iter
				{
					let mut local_lookup = local_lookup.clone();

					let simplyfied_type = add_to_generic_lookup(
						expression_type,
						function_type,
						&mut local_lookup
					);
					if simplyfied_type.is_none()
					{
						return Ok((None, typed_ast));
					}
					let simplyfied_type = simplyfied_type.unwrap();
					let (returned_type, new_ast) = get_signatures(
						child,
						args.clone(),
						args_names,
						local_lookup,
						function_definition,
						definition_arg_names,
						definition_types,
						context,
						typed_ast,
					)?;
					typed_ast = new_ast;
					match returned_type
					{
						None => continue,
						Some(new_node) => {
							new_node
								.iter()
								.fold(
									&mut trie_node,
									|trie_node, (down_key, value)|
									{
										let mut down_key = down_key
											.into_iter()
											.cloned()
											.collect::<VecDeque<Type>>();
										down_key.push_front(simplyfied_type.clone());
										trie_node.insert_owned(
											down_key,
											value.clone(),
										);
										trie_node
									}
								);
						}
					}
				}
			}
			if trie_node.is_empty()
			{
				return Ok((
					None,
					typed_ast,
				));
			}
			return Ok((
				Some(trie_node),
				typed_ast,
			));
		}
	}
}

// fn type_the_definition(
// 	definition: &Definition,
// 	definition_arg_names: &HashMap<Identifier, String>,
// 	definition_type: &mut Vec<Type>,
// 	context: &Context<Ast>,
// 	typed_ast: TypedAst,
// ) -> Result<TypedDefinition, TypeErr>
// {
// 	match definition
// 	{
// 		Definition::BuiltIn(built_in) => Ok(
// 			TypedDefinition::BuiltIn(
// 				built_in.clone(),
// 				built_in_type(&built_in)
// 			)
// 		),
// 		Definition::FirstClass(constant) =>
// 		{
// 			let (type_, new_ast) = constant_type(
// 				constant,
// 				context,
// 				typed_ast
// 			)?;
// 			return Ok(
// 				TypedDefinition::FirstClass(
// 					constant.clone(),
// 					type_,
// 				)
// 			);
// 		},
// 		Definition::Definition(function_call) =>
// 		{
// 			todo!()
// 		}
// 	}
// }

fn build_definition_type(
	definition_arg_names: &HashMap<Identifier, String>,
	type_generics_lookup: &HashMap<String, HashSet<Type>>,
	function_definition: &FunctionDefinition,
	return_type: Type,
) -> Type
{
	let mut signature_type_vec: Vec<Type> = Vec::with_capacity(
		function_definition
			.args
			.len() + 1
	);
	for arg_name in function_definition
		.args
		.iter()
	{
		let generic_name = definition_arg_names
			.get(&arg_name)
			.unwrap();
		let type_ = type_generics_lookup
			.get(generic_name)
			.unwrap()
			.iter()
			.next()
			.unwrap();
		signature_type_vec.push(type_.clone());
	}
	let return_type = simplify_generic(
		return_type,
		type_generics_lookup,
	);
	signature_type_vec.push(return_type);
	Type::from(signature_type_vec)
}

fn simplify_generic(
	type_: Type,
	generic_lookup: &HashMap<String, HashSet<Type>>,
) -> Type
{
	if !type_.is_generic()
	{
		return type_;
	}
	match type_
	{
		Type::Generic(ref type_identifier) =>
		{
			let other_type = generic_lookup
				.get(&type_identifier.string)
				.unwrap()
				.iter()
				.cloned()
				.next();
			return other_type.unwrap_or(type_)
		},
		Type::Signature(args_types, return_type) =>
		{
			let mut type_vec = Vec::with_capacity(args_types.len() +1);
			for arg_type in args_types.into_iter()
			{
				let type_ = simplify_generic(arg_type, generic_lookup);
				type_vec.push(type_);
			}
			let type_ = simplify_generic(*return_type, generic_lookup);
			type_vec.push(type_);
			return Type::from(type_vec);
		},
		_ => panic!()
	}
}

fn not_infered(
	function_call: &FunctionCall,
	args_names: &HashMap<Identifier, String>,
	args_types: &mut HashMap<String, HashSet<Type>>,
	function_definition: &FunctionDefinition,
	definition_type: &mut Vec<Type>,
	context: &Context<Ast>,
	typed_ast: TypedAst,
	function_name: &String,
	is_last_expression: bool,
) -> Result<(AmbiguousType, TypedAst), TypeErr>
{
	match (*context).get(function_name)
	{
		Some(overloads) =>
		{
			let new_ast = infer(
				overloads,
				context,
				typed_ast,
				function_name,
			)?;
			check_function_call(
				function_call,
				args_names,
				args_types,
				function_definition,
				definition_type,
				context,
				new_ast,
				is_last_expression,
			)
		},
		None => infer_generics(
			function_call,
			args_names,
			args_types,
			function_definition,
			definition_type,
			context,
			typed_ast,
			is_last_expression
		)
	}
}

fn infer(
	overloads: &Overloads,
	context: &Context<Ast>,
	mut typed_ast: TypedAst,
	function_name: &String,
) -> Result<TypedAst, TypeErr>
{
	let mut typed_overloads = SequenceTrie::new();
	for definition in overloads.iter()
	{
		let (definitions, new_typed_ast) = get_definition_type(
			definition,
			context,
			typed_ast
		)?;

		typed_ast = new_typed_ast;
		let new_typed_overloads = definitions
			.into_iter()
			.fold(
				typed_overloads,
				|mut trie, typed_definition|
				{
					let type_ = match typed_definition.clone()
					{
						TypedDefinition::BuiltIn(_, type_) => type_,
						TypedDefinition::Definition(_, type_) => type_,
						TypedDefinition::FirstClass(_, type_) => type_,
					};
					trie.insert(&type_, typed_definition);
					trie
				}
			);
		typed_overloads = new_typed_overloads;
	}
	typed_ast.insert(
		function_name.clone(),
		typed_overloads
	);
	Ok(typed_ast)
}

fn infer_generics(
	function_call: &FunctionCall,
	definition_args_names: &HashMap<Identifier, String>,
	generic_lookup: &mut HashMap<String, HashSet<Type>>,
	function_definition: &FunctionDefinition,
	definition_types: &mut Vec<Type>,
	context: &Context<Ast>,
	typed_ast: TypedAst,
	is_last_expression: bool,
) -> Result<(AmbiguousType, TypedAst), TypeErr>
{
	let generic_name = definition_args_names
		.get(&function_call.identifier)
		.unwrap();
	let mut type_id = Identifier::from(vec![generic_name]);
	type_id.location = function_call
		.identifier
		.location
		.clone();
	let type_ = Type::Generic(
		type_id
	);
	let arity = function_call.args.len();
	if arity == 0
	{
		match generic_lookup.get_mut(generic_name)
		{
			Some(_types) =>
			{
				todo!()
			},
			None =>
			{
				let mut hash = HashSet::new();
				hash.insert(type_.clone());
				generic_lookup.insert(
					generic_name.clone(),
					hash
				);
				let type_ = build_type(
					function_definition,
					definition_args_names,
					generic_lookup,
					type_
				)?;
				definition_types.push(type_.clone());
				return Ok((
					AmbiguousType::NotAmbiguous(
						type_
					),
					typed_ast
				))
			}
		}
	}
	let (mut type_vec, new_typed_ast) = function_call
		.args
		.iter()
		.fold(
			Ok((Vec::with_capacity(arity), typed_ast)),
			|acc, expression|
			{
				println!("entered here");
				let (mut type_vec, typed_ast) = acc?;
				let (types, new_ast) = get_expression_type(
					expression,
					definition_args_names,
					generic_lookup,
					function_definition,
					definition_types,
					context,
					typed_ast,
					false,
				)?;
				let expression_type_vec = <&AmbiguousType as Into<Vec<Type>>>::into(&types);
				for type_ in expression_type_vec.iter()
				{
					if let Type::Generic(type_identifer) = type_
					{
						match generic_lookup.get_mut(&type_identifer.string)
						{
							Some(found) =>
							{
								println!(
									"generic type identifier: {:?} -> {:?}",
									type_identifer,
									found
								);
								// if !type_match(found, type_)
								// {
								// 	todo!()
								// }
							},
							None =>
							{
								let mut hash = HashSet::new();
								hash.insert(type_.clone());
								generic_lookup.insert(
									type_identifer.string.clone(),
									hash
								);
							}
						}
					}
				}
				type_vec.push(types);
				Ok((type_vec, new_ast))
			}
		)?;
	type_vec.push(AmbiguousType::NotAmbiguous(type_.clone()));
	let function_types = expanded_types(
		type_vec
	).into_iter()
		.map(|signature| Type::from(signature))
		.fold(
			HashSet::new(),
			|mut hash_set, type_|
			{
				hash_set.insert(type_);
				hash_set
			});
	match generic_lookup.get_mut(generic_name)
	{
		None =>
		{
			generic_lookup.insert(
				generic_name.clone(),
				function_types
			);
		},
		Some(types) =>
		{
			let intersection: HashSet<Type> = types
				.intersection(&function_types)
				.cloned()
				.collect();
			if intersection.is_empty()
			{
				panic!();
			}
			*types = intersection;
		}
	}
	if is_last_expression
	{
		println!(
			"generic look up: {:#?}",
			generic_lookup,
		);
		println!(
			"args look up: {:#?}",
			definition_args_names,
		);
		let built_type = build_type(
			function_definition,
			definition_args_names,
			generic_lookup,
			type_.clone()
		)?;
		definition_types.push(built_type.clone());
	}
	return Ok((
		AmbiguousType::NotAmbiguous(
			type_
		),
		new_typed_ast
	))
}

fn add_to_generic_lookup(
	expression_type: &Type,
	function_type: &Type,
	type_generic_lookup: &mut HashMap<String, HashSet<Type>>,
) -> Option<Type>
{
	match (expression_type, function_type)
	{
		(Type::Generic(expression_identifier), function_type) =>
		{
			match type_generic_lookup.get_mut(&expression_identifier.string)
			{
				Some(found) => {
					// if !type_match(found, function_type)
					// {
					// 	return None
					// }
					let mut hash = HashSet::with_capacity(1);
					hash.insert(function_type.clone());
					let intersection:HashSet<Type> = found
						.intersection(&hash)
						.cloned()
						.collect();
					if intersection.is_empty()
					{
						panic!()
					}
					*found = intersection;
					return Some(function_type.clone())
				},
				None =>
				{
					let mut hash = HashSet::new();
					hash.insert(function_type.clone());
					type_generic_lookup.insert(
						expression_identifier.string.clone(),
						hash
					);
					return Some(function_type.clone())
				}
			}
		},
		(expression_type, Type::Generic(function_identifier)) =>
		{
			match type_generic_lookup.get_mut(&function_identifier.string)
			{
				Some(found) =>
				{
					let mut hash = HashSet::with_capacity(1);
					hash.insert(expression_type.clone());
					let intersection:HashSet<Type> = found
						.intersection(&hash)
						.cloned()
						.collect();
					if intersection.is_empty()
					{
						panic!()
					}
					*found = intersection;
					return Some(expression_type.clone())
				}
				None =>
				{
					let mut hash = HashSet::new();
					hash.insert(expression_type.clone());
					type_generic_lookup.insert(
						function_identifier.string.clone(),
						hash
					);
					return Some(expression_type.clone())
				}
			}
		},
		(expression_type@Type::Signature(expression_args, expression_return),
		 function_type@Type::Signature(function_args, function_return)) =>
		{
			let mut simplified_type = Vec::with_capacity(function_args.len());
			if !expression_type.is_generic() &&
				function_type.is_generic()
			{
				let mut simplified_type = Vec::with_capacity(function_args.len());
				for (function_arg, expression_arg) in function_args
					.iter()
					.zip(expression_args.iter())
				{
					let type_ = add_to_generic_lookup(
						expression_arg,
						function_arg,
						type_generic_lookup
					)?;
					simplified_type.push(type_);
				}
				let type_ = add_to_generic_lookup(
					expression_return,
					function_return,
					type_generic_lookup
				)?;
				simplified_type.push(type_);
				return Some(Type::from(simplified_type))
			}
			for (function_arg, expression_arg) in function_args
				.iter()
				.zip(expression_args.iter())
			{
				let type_ = add_to_generic_lookup(
					expression_arg,
					function_arg,
					type_generic_lookup
				)?;
				simplified_type.push(type_);
			}
			let type_ = add_to_generic_lookup(
				expression_return,
				function_return,
				type_generic_lookup
			)?;
			simplified_type.push(type_);
			return Some(Type::from(simplified_type))
		},
		(type_, _) => return Some(type_.clone())
	}
}

fn expanded_types(
	mut ambiguous: Vec<AmbiguousType>
) -> Vec<Vec<Type>>
{
	let mut return_vec = Vec::new();
	if ambiguous.len() == 1
	{
		match &ambiguous[0]
		{
			AmbiguousType::NotAmbiguous(type_) => return_vec.push(vec![type_.clone()]),
			AmbiguousType::Ambiguous(type_vec) => return_vec.push(type_vec.clone()),
		}
		return return_vec
	}
	let current = ambiguous.remove(0);
	let expanded = expanded_types(ambiguous);
	match current
	{
		AmbiguousType::NotAmbiguous(current_type) =>
		{
			for type_ in expanded.into_iter()
			{
				let vec = vec![current_type.clone()]
					.into_iter()
					.chain(type_.into_iter())
					.collect();
				return_vec.push(vec);
			}
		},
		AmbiguousType::Ambiguous(type_vec) =>
		{
			for current_type in type_vec
			{
				for type_ in expanded.clone().into_iter()
				{
					let vec = vec![current_type.clone()]
						.into_iter()
						.chain(type_.into_iter())
						.collect();
					return_vec.push(vec);
				}
			}
		}
	}
	return_vec
}

fn build_type(
	function_definition: &FunctionDefinition,
	args_names: &HashMap<Identifier, String>,
	args_types: &HashMap<String, HashSet<Type>>,
	return_type: Type,
) -> Result<Type, TypeErr>
{
	let arity = function_definition.args.len();
	let mut args_types_vec: Vec<Type> = Vec::with_capacity(arity);

	for arg_identifier in function_definition
		.args
		.iter()
	{
		let generic_name = args_names
			.get(arg_identifier)
			.unwrap();
		let types = args_types
			.get(generic_name)
			.unwrap();
		if types.len() != 1
		{
			panic!()
		}
		args_types_vec.push(
			types
				.iter()
				.next()
				.cloned()
				.unwrap()
		)
	}
	Ok(Type::Signature(args_types_vec, Box::new(return_type)))
}

fn type_match(
	l: &Type,
	r: &Type
) -> bool
{
	match (l, r)
	{
		(&Type::Generic(_), _) => true,
		(_, &Type::Generic(_)) => true,
		(&Type::Type(ref t_l), &Type::Type(ref t_r)) => t_l.string == t_r.string,
		(&Type::Signature(ref args_l, ref return_l), &Type::Signature(ref args_r, ref return_r)) =>
		{
			let return_eq = type_match(
				&(*return_l),
				&(*return_r)
			);
			let args_eq = args_l
				.iter()
				.zip(args_r.iter()
				).all(|(l, r)| type_match(l, r));
			args_eq && return_eq
		}
		_ => false
	}
}

fn constant_type(
	constant: &Constant,
	context: &Context<Ast>,
	typed_ast: TypedAst,
) -> Result<(AmbiguousType, TypedAst), TypeErr>
{
	match constant
	{
		Constant::Int(_, location) => {
			let mut id = Identifier::from(&vec!["Int"]);
			id.location = Some(location.clone());
			Ok((
				AmbiguousType::NotAmbiguous(
					Type::Type(id)
				),
				typed_ast,
			))
		},
		Constant::Identifier(identifier) =>
		{
			match typed_ast.get(&identifier.string)
			{
				Some(overloads) =>
				{
					let types: Vec<Type> = overloads
						.keys()
						.map(|type_| Type::from(type_))
						.collect();
					Ok(
						(AmbiguousType::from(types),
						 typed_ast
						))
				},
				None =>
				{
					match context.get(&identifier.string)
					{
						None => panic!(),
						Some(overloads) =>
						{
							let new_ast = infer(
								overloads,
								context,
								typed_ast,
								&identifier.string,
							)?;
							constant_type(
								constant,
								context,
								new_ast
							)
						}
					}
				}
			}
		},
	}
}

pub fn built_in_type(
	built_in: &BuiltIn
) -> Type
{
	match built_in
	{
		BuiltIn::AddBinOpInt => Type::Signature(
			vec![
				Type::Type(Identifier::from(&vec!["Int"])),
				Type::Type(Identifier::from(&vec!["Int"])),
			],
			Box::new(Type::Type(Identifier::from(&vec!["Int"])))
		)
	}
}

fn default_typed_ast() -> TypedAst
{
	let mut ast = HashMap::new();

	ast.insert(
		"+".to_string(),
		add_overloads());
	ast
}

fn add_overloads() -> TypedOverloads
{
	let mut add_overloads = SequenceTrie::new();

	let add_type = built_in_type(&BuiltIn::AddBinOpInt);
	add_overloads.insert(
		add_type.clone().into_iter(),
		TypedDefinition::BuiltIn(
			BuiltIn::AddBinOpInt,
			add_type
		));
	add_overloads
}
